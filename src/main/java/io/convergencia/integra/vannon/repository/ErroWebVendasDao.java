/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.repository;

import io.convergencia.integra.vannon.model.ErroWebVenda;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author wanderson
 */
@RepositoryRestResource(collectionResourceRel = "erro-web-vendas", path = "erro-web-venda")
public interface ErroWebVendasDao extends PagingAndSortingRepository<ErroWebVenda, Long> {

    List<ErroWebVenda> findTop150ByOrderByIdDesc();
}

package io.convergencia.integra.vannon.model;

import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ValorParcela",
    "NumeroParcela",
    "DataVencimento",
    "ValorDinheiro",
    "ValorTroco",
    "NuAutorizacao",
    "NuComprovante",
    "NumeroBinCartao",
    "Pagamento"
})
public class PedidoPagamento {

    @JsonProperty("ValorParcela")
    private Double valorParcela;
    @JsonProperty("NumeroParcela")
    private Integer numeroParcela;
    @JsonProperty("DataVencimento")
    private String dataVencimento;
    @JsonProperty("ValorDinheiro")
    private Double valorDinheiro;
    @JsonProperty("ValorTroco")
    private Double valorTroco;
    @JsonProperty("NuAutorizacao")
    private String nuAutorizacao;
    @JsonProperty("NuComprovante")
    private String nuComprovante;
    @JsonProperty("NumeroBinCartao")
    private String numeroBinCartao;
    @JsonProperty("Pagamento")
    @Valid
    private Pagamento pagamento;

    @JsonProperty("ValorParcela")
    public Double getValorParcela() {
        return valorParcela;
    }

    @JsonProperty("ValorParcela")
    public void setValorParcela(Double valorParcela) {
        this.valorParcela = valorParcela;
    }

    @JsonProperty("NumeroParcela")
    public Integer getNumeroParcela() {
        return numeroParcela;
    }

    @JsonProperty("NumeroParcela")
    public void setNumeroParcela(Integer numeroParcela) {
        this.numeroParcela = numeroParcela;
    }

    @JsonProperty("DataVencimento")
    public String getDataVencimento() {
        return dataVencimento;
    }

    @JsonProperty("DataVencimento")
    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    @JsonProperty("ValorDinheiro")
    public Double getValorDinheiro() {
        return valorDinheiro;
    }

    @JsonProperty("ValorDinheiro")
    public void setValorDinheiro(Double valorDinheiro) {
        this.valorDinheiro = valorDinheiro;
    }

    @JsonProperty("ValorTroco")
    public Double getValorTroco() {
        return valorTroco;
    }

    @JsonProperty("ValorTroco")
    public void setValorTroco(Double valorTroco) {
        this.valorTroco = valorTroco;
    }

    @JsonProperty("NuAutorizacao")
    public String getNuAutorizacao() {
        return nuAutorizacao;
    }

    @JsonProperty("NuAutorizacao")
    public void setNuAutorizacao(String nuAutorizacao) {
        this.nuAutorizacao = nuAutorizacao;
    }

    @JsonProperty("NuComprovante")
    public String getNuComprovante() {
        return nuComprovante;
    }

    @JsonProperty("NuComprovante")
    public void setNuComprovante(String nuComprovante) {
        this.nuComprovante = nuComprovante;
    }

    @JsonProperty("NumeroBinCartao")
    public String getNumeroBinCartao() {
        return numeroBinCartao;
    }

    @JsonProperty("NumeroBinCartao")
    public void setNumeroBinCartao(String numeroBinCartao) {
        this.numeroBinCartao = numeroBinCartao;
    }

    @JsonProperty("Pagamento")
    public Pagamento getPagamento() {
        return pagamento;
    }

    @JsonProperty("Pagamento")
    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

}

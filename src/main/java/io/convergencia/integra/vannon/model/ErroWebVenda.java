/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.model;

import io.convergencia.integra.ecommerce.model.Entidade;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author wanderson
 */
@Entity
@Table(name = "INTEGRA_ECOMMERCE_ERRO_WEB_VENDA")
@DynamicUpdate
public class ErroWebVenda extends Entidade{
    
    @Column(name = "OPERACAO")
    private String operacao;
    
    @Column(name = "URL")
    private String url;
    
    @Lob
    @Column(name = "DATA_SEND")
    private String dataSend;
    
    @Lob
    @Column(name = "DATA_RETURN")
    private String dataReturn;
    
    @Column(name = "ERRO")
    private String erro;

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDataSend() {
        return dataSend;
    }

    public void setDataSend(String dataSend) {
        this.dataSend = dataSend;
    }

    public String getDataReturn() {
        return dataReturn;
    }

    public void setDataReturn(String dataReturn) {
        this.dataReturn = dataReturn;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }
    
    
    
}

package io.convergencia.integra.vannon.model;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CodigoMarca",
    "CodigoProdutoTipo",
    "CodigoProdutoControle",
    "CodigoProdutoFamilia",
    "Ativo",
    "PreVenda",
    "Lancamento",
    "CodigoExterno",
    "DataPreVenda",
    "DataLancamentoInicial",
    "DataLancamentoFinal",
    "NomeProduto",
    "PalavraChave",
    "DescricaoCurta",
    "ArquivoBula",
    "CurvaAbc",
    "UrlVideo"
})
public class Produto {

    @JsonProperty("CodigoMarca")
    private String codigoMarca;
    @JsonProperty("CodigoProdutoTipo")
    private Long codigoProdutoTipo;
    @JsonProperty("CodigoProdutoControle")
    private Long codigoProdutoControle;
    @JsonProperty("CodigoProdutoFamilia")
    private Long codigoProdutoFamilia;
    @JsonProperty("Ativo")
    private Boolean ativo;
    @JsonProperty("PreVenda")
    private Boolean preVenda;
    @JsonProperty("Lancamento")
    private Boolean lancamento;
    @JsonProperty("CodigoExterno")
    private String codigoExterno;
    @JsonProperty("DataPreVenda")
    private String dataPreVenda;
    @JsonProperty("DataLancamentoInicial")
    private String dataLancamentoInicial;
    @JsonProperty("DataLancamentoFinal")
    private String dataLancamentoFinal;
    @JsonProperty("NomeProduto")
    private String nomeProduto;
    @JsonProperty("PalavraChave")
    private String palavraChave;
    @JsonProperty("DescricaoCurta")
    private String descricaoCurta;
    @JsonProperty("ArquivoBula")
    private String arquivoBula;
    @JsonProperty("CurvaAbc")
    private String curvaAbc;
    @JsonProperty("UrlVideo")
    private String urlVideo;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CodigoMarca")
    public String getCodigoMarca() {
        return codigoMarca;
    }

    @JsonProperty("CodigoMarca")
    public void setCodigoMarca(String codigoMarca) {
        this.codigoMarca = codigoMarca;
    }

    @JsonProperty("CodigoProdutoTipo")
    public Long getCodigoProdutoTipo() {
        return codigoProdutoTipo;
    }

    @JsonProperty("CodigoProdutoTipo")
    public void setCodigoProdutoTipo(Long codigoProdutoTipo) {
        this.codigoProdutoTipo = codigoProdutoTipo;
    }

    @JsonProperty("CodigoProdutoControle")
    public Long getCodigoProdutoControle() {
        return codigoProdutoControle;
    }

    @JsonProperty("CodigoProdutoControle")
    public void setCodigoProdutoControle(Long codigoProdutoControle) {
        this.codigoProdutoControle = codigoProdutoControle;
    }

    @JsonProperty("CodigoProdutoFamilia")
    public Long getCodigoProdutoFamilia() {
        return codigoProdutoFamilia;
    }

    @JsonProperty("CodigoProdutoFamilia")
    public void setCodigoProdutoFamilia(Long codigoProdutoFamilia) {
        this.codigoProdutoFamilia = codigoProdutoFamilia;
    }

    @JsonProperty("Ativo")
    public Boolean getAtivo() {
        return ativo;
    }

    @JsonProperty("Ativo")
    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @JsonProperty("PreVenda")
    public Boolean getPreVenda() {
        return preVenda;
    }

    @JsonProperty("PreVenda")
    public void setPreVenda(Boolean preVenda) {
        this.preVenda = preVenda;
    }

    @JsonProperty("Lancamento")
    public Boolean getLancamento() {
        return lancamento;
    }

    @JsonProperty("Lancamento")
    public void setLancamento(Boolean lancamento) {
        this.lancamento = lancamento;
    }

    @JsonProperty("CodigoExterno")
    public String getCodigoExterno() {
        return codigoExterno;
    }

    @JsonProperty("CodigoExterno")
    public void setCodigoExterno(String codigoExterno) {
        this.codigoExterno = codigoExterno;
    }

    @JsonProperty("DataPreVenda")
    public String getDataPreVenda() {
        return dataPreVenda;
    }

    @JsonProperty("DataPreVenda")
    public void setDataPreVenda(String dataPreVenda) {
        this.dataPreVenda = dataPreVenda;
    }

    @JsonProperty("DataLancamentoInicial")
    public String getDataLancamentoInicial() {
        return dataLancamentoInicial;
    }

    @JsonProperty("DataLancamentoInicial")
    public void setDataLancamentoInicial(String dataLancamentoInicial) {
        this.dataLancamentoInicial = dataLancamentoInicial;
    }

    @JsonProperty("DataLancamentoFinal")
    public String getDataLancamentoFinal() {
        return dataLancamentoFinal;
    }

    @JsonProperty("DataLancamentoFinal")
    public void setDataLancamentoFinal(String dataLancamentoFinal) {
        this.dataLancamentoFinal = dataLancamentoFinal;
    }

    @JsonProperty("NomeProduto")
    public String getNomeProduto() {
        return nomeProduto;
    }

    @JsonProperty("NomeProduto")
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    @JsonProperty("PalavraChave")
    public String getPalavraChave() {
        return palavraChave;
    }

    @JsonProperty("PalavraChave")
    public void setPalavraChave(String palavraChave) {
        this.palavraChave = palavraChave;
    }

    @JsonProperty("DescricaoCurta")
    public String getDescricaoCurta() {
        return descricaoCurta;
    }

    @JsonProperty("DescricaoCurta")
    public void setDescricaoCurta(String descricaoCurta) {
        this.descricaoCurta = descricaoCurta;
    }

    @JsonProperty("ArquivoBula")
    public String getArquivoBula() {
        return arquivoBula;
    }

    @JsonProperty("ArquivoBula")
    public void setArquivoBula(String arquivoBula) {
        this.arquivoBula = arquivoBula;
    }

    @JsonProperty("CurvaAbc")
    public String getCurvaAbc() {
        return curvaAbc;
    }

    @JsonProperty("CurvaAbc")
    public void setCurvaAbc(String curvaAbc) {
        this.curvaAbc = curvaAbc;
    }

    @JsonProperty("UrlVideo")
    public String getUrlVideo() {
        return urlVideo;
    }

    @JsonProperty("UrlVideo")
    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

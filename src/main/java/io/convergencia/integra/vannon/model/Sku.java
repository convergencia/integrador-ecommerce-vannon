package io.convergencia.integra.vannon.model;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CodigoProduto",
    "Brinde",
    "CodigoExterno",
    "CodigoBarras",
    "Peso",
    "Altura",
    "Largura",
    "Profundidade",
    "Nome",
    "CodigoMS"
})
public class Sku {

    @JsonProperty("CodigoProduto")
    private String codigoProduto;
    @JsonProperty("Brinde")
    private Boolean brinde;
    @JsonProperty("CodigoExterno")
    private String codigoExterno;
    @JsonProperty("CodigoBarras")
    private String codigoBarras;
    @JsonProperty("Peso")
    private Float peso;
    @JsonProperty("Altura")
    private Long altura;
    @JsonProperty("Largura")
    private Long largura;
    @JsonProperty("Profundidade")
    private Long profundidade;
    @JsonProperty("Nome")
    private String nome;
    @JsonProperty("CodigoMS")
    private String codigoMS;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CodigoProduto")
    public String getCodigoProduto() {
        return codigoProduto;
    }

    @JsonProperty("CodigoProduto")
    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    @JsonProperty("Brinde")
    public Boolean getBrinde() {
        return brinde;
    }

    @JsonProperty("Brinde")
    public void setBrinde(Boolean brinde) {
        this.brinde = brinde;
    }

    @JsonProperty("CodigoExterno")
    public String getCodigoExterno() {
        return codigoExterno;
    }

    @JsonProperty("CodigoExterno")
    public void setCodigoExterno(String codigoExterno) {
        this.codigoExterno = codigoExterno;
    }

    @JsonProperty("CodigoBarras")
    public String getCodigoBarras() {
        return codigoBarras;
    }

    @JsonProperty("CodigoBarras")
    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    @JsonProperty("Peso")
    public Float getPeso() {
        return peso;
    }

    @JsonProperty("Peso")
    public void setPeso(Float peso) {
        this.peso = peso;
    }

    @JsonProperty("Altura")
    public Long getAltura() {
        return altura;
    }

    @JsonProperty("Altura")
    public void setAltura(Long altura) {
        this.altura = altura;
    }

    @JsonProperty("Largura")
    public Long getLargura() {
        return largura;
    }

    @JsonProperty("Largura")
    public void setLargura(Long largura) {
        this.largura = largura;
    }

    @JsonProperty("Profundidade")
    public Long getProfundidade() {
        return profundidade;
    }

    @JsonProperty("Profundidade")
    public void setProfundidade(Long profundidade) {
        this.profundidade = profundidade;
    }

    @JsonProperty("Nome")
    public String getNome() {
        return nome;
    }

    @JsonProperty("Nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonProperty("CodigoMS")
    public String getCodigoMS() {
        return codigoMS;
    }

    @JsonProperty("CodigoMS")
    public void setCodigoMS(String codigoMS) {
        this.codigoMS = codigoMS;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package io.convergencia.integra.vannon.model;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroPedido",
    "PrazoEntrega",
    "DataPedido",
    "Total",
    "Frete",
    "Desconto",
    "Cep",
    "Rastreamento",
    "CodigoVendedor",
    "Integracao",
    "ModalidadeEntrega",
    "CodigoPraca",
    "Cliente",
    "PedidoEntrega",
    "PedidoPagamentos",
    "PedidoProdutoSku",
    "PedidoStatus"
})
public class Pedido {

    @JsonProperty("NumeroPedido")
    private String numeroPedido;
    @JsonProperty("PrazoEntrega")
    private Integer prazoEntrega;
    @JsonProperty("DataPedido")
    private String dataPedido;
    @JsonProperty("Total")
    private Double total;
    @JsonProperty("Frete")
    private Double frete;
    @JsonProperty("Desconto")
    private Double desconto;
    @JsonProperty("Cep")
    private String cep;
    @JsonProperty("Rastreamento")
    private String rastreamento;
    @JsonProperty("CodigoVendedor")
    private String codigoVendedor;
    @JsonProperty("Integracao")
    private String integracao;
    @JsonProperty("ModalidadeEntrega")
    private String modalidadeEntrega;
    @JsonProperty("CodigoPraca")
    private String codigoPraca;
    @JsonProperty("Cliente")
    @Valid
    private Cliente cliente;
    @JsonProperty("PedidoEntrega")
    @Valid
    private List<PedidoEntrega> pedidoEntrega = new ArrayList<PedidoEntrega>();
    @JsonProperty("PedidoPagamentos")
    @Valid
    private List<PedidoPagamento> pedidoPagamentos = new ArrayList<PedidoPagamento>();
    @JsonProperty("PedidoProdutoSku")
    @Valid
    private List<PedidoProdutoSku> pedidoProdutoSku = new ArrayList<PedidoProdutoSku>();
    @JsonProperty("PedidoStatus")
    @Valid
    private PedidoStatus pedidoStatus;

    @JsonProperty("NumeroPedido")
    public String getNumeroPedido() {
        return numeroPedido;
    }

    @JsonProperty("NumeroPedido")
    public void setNumeroPedido(String numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    @JsonProperty("PrazoEntrega")
    public Integer getPrazoEntrega() {
        return prazoEntrega;
    }

    @JsonProperty("PrazoEntrega")
    public void setPrazoEntrega(Integer prazoEntrega) {
        this.prazoEntrega = prazoEntrega;
    }

    @JsonProperty("DataPedido")
    public String getDataPedido() {
        return dataPedido;
    }

    @JsonProperty("DataPedido")
    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    @JsonProperty("Total")
    public Double getTotal() {
        return total;
    }

    @JsonProperty("Total")
    public void setTotal(Double total) {
        this.total = total;
    }

    @JsonProperty("Frete")
    public Double getFrete() {
        return frete;
    }

    @JsonProperty("Frete")
    public void setFrete(Double frete) {
        this.frete = frete;
    }

    @JsonProperty("Desconto")
    public Double getDesconto() {
        return desconto;
    }

    @JsonProperty("Desconto")
    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    @JsonProperty("Cep")
    public String getCep() {
        return cep;
    }

    @JsonProperty("Cep")
    public void setCep(String cep) {
        this.cep = cep;
    }

    @JsonProperty("Rastreamento")
    public String getRastreamento() {
        return rastreamento;
    }

    @JsonProperty("Rastreamento")
    public void setRastreamento(String rastreamento) {
        this.rastreamento = rastreamento;
    }

    @JsonProperty("CodigoVendedor")
    public String getCodigoVendedor() {
        return codigoVendedor;
    }

    @JsonProperty("CodigoVendedor")
    public void setCodigoVendedor(String codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    @JsonProperty("Integracao")
    public String getIntegracao() {
        return integracao;
    }

    @JsonProperty("Integracao")
    public void setIntegracao(String integracao) {
        this.integracao = integracao;
    }

    @JsonProperty("ModalidadeEntrega")
    public String getModalidadeEntrega() {
        return modalidadeEntrega;
    }

    @JsonProperty("ModalidadeEntrega")
    public void setModalidadeEntrega(String modalidadeEntrega) {
        this.modalidadeEntrega = modalidadeEntrega;
    }

    @JsonProperty("CodigoPraca")
    public String getCodigoPraca() {
        return codigoPraca;
    }

    @JsonProperty("CodigoPraca")
    public void setCodigoPraca(String codigoPraca) {
        this.codigoPraca = codigoPraca;
    }

    @JsonProperty("Cliente")
    public Cliente getCliente() {
        return cliente;
    }

    @JsonProperty("Cliente")
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @JsonProperty("PedidoEntrega")
    public List<PedidoEntrega> getPedidoEntrega() {
        return pedidoEntrega;
    }

    @JsonProperty("PedidoEntrega")
    public void setPedidoEntrega(List<PedidoEntrega> pedidoEntrega) {
        this.pedidoEntrega = pedidoEntrega;
    }

    @JsonProperty("PedidoPagamentos")
    public List<PedidoPagamento> getPedidoPagamentos() {
        return pedidoPagamentos;
    }

    @JsonProperty("PedidoPagamentos")
    public void setPedidoPagamentos(List<PedidoPagamento> pedidoPagamentos) {
        this.pedidoPagamentos = pedidoPagamentos;
    }

    @JsonProperty("PedidoProdutoSku")
    public List<PedidoProdutoSku> getPedidoProdutoSku() {
        return pedidoProdutoSku;
    }

    @JsonProperty("PedidoProdutoSku")
    public void setPedidoProdutoSku(List<PedidoProdutoSku> pedidoProdutoSku) {
        this.pedidoProdutoSku = pedidoProdutoSku;
    }

    @JsonProperty("PedidoStatus")
    public PedidoStatus getPedidoStatus() {
        return pedidoStatus;
    }

    @JsonProperty("PedidoStatus")
    public void setPedidoStatus(PedidoStatus pedidoStatus) {
        this.pedidoStatus = pedidoStatus;
    }

}

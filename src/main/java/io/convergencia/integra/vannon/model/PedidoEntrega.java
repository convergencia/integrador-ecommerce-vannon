package io.convergencia.integra.vannon.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Endereco",
    "Numero",
    "Complemento",
    "Referencia",
    "Bairro",
    "Cidade",
    "Estado",
    "Destinatario",
    "TipoEndereco"
})
public class PedidoEntrega {

    @JsonProperty("Endereco")
    private String endereco;
    @JsonProperty("Numero")
    private String numero;
    @JsonProperty("Complemento")
    private String complemento;
    @JsonProperty("Referencia")
    private String referencia;
    @JsonProperty("Bairro")
    private String bairro;
    @JsonProperty("Cidade")
    private String cidade;
    @JsonProperty("Estado")
    private String estado;
    @JsonProperty("Destinatario")
    private String destinatario;
    @JsonProperty("TipoEndereco")
    private String tipoEndereco;

    @JsonProperty("Endereco")
    public String getEndereco() {
        return endereco;
    }

    @JsonProperty("Endereco")
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    @JsonProperty("Numero")
    public String getNumero() {
        return numero;
    }

    @JsonProperty("Numero")
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @JsonProperty("Complemento")
    public String getComplemento() {
        return complemento;
    }

    @JsonProperty("Complemento")
    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @JsonProperty("Referencia")
    public String getReferencia() {
        return referencia;
    }

    @JsonProperty("Referencia")
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @JsonProperty("Bairro")
    public String getBairro() {
        return bairro;
    }

    @JsonProperty("Bairro")
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    @JsonProperty("Cidade")
    public String getCidade() {
        return cidade;
    }

    @JsonProperty("Cidade")
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @JsonProperty("Estado")
    public String getEstado() {
        return estado;
    }

    @JsonProperty("Estado")
    public void setEstado(String estado) {
        this.estado = estado;
    }

    @JsonProperty("Destinatario")
    public String getDestinatario() {
        return destinatario;
    }

    @JsonProperty("Destinatario")
    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    @JsonProperty("TipoEndereco")
    public String getTipoEndereco() {
        return tipoEndereco;
    }

    @JsonProperty("TipoEndereco")
    public void setTipoEndereco(String tipoEndereco) {
        this.tipoEndereco = tipoEndereco;
    }

}

package io.convergencia.integra.vannon.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CodigoProdutoSku",
    "Nome",
    "Quantidade",
    "ValorProduto",
    "ValorReal",
    "ValorEmbalagem"
})
public class PedidoProdutoSku {

    @JsonProperty("CodigoProdutoSku")
    private String codigoProdutoSku;
    @JsonProperty("Nome")
    private String nome;
    @JsonProperty("Quantidade")
    private Integer quantidade;
    @JsonProperty("ValorProduto")
    private Double valorProduto;
    @JsonProperty("ValorReal")
    private Double valorReal;
    @JsonProperty("ValorEmbalagem")
    private Double valorEmbalagem;

    @JsonProperty("CodigoProdutoSku")
    public String getCodigoProdutoSku() {
        return codigoProdutoSku;
    }

    @JsonProperty("CodigoProdutoSku")
    public void setCodigoProdutoSku(String codigoProdutoSku) {
        this.codigoProdutoSku = codigoProdutoSku;
    }

    @JsonProperty("Nome")
    public String getNome() {
        return nome;
    }

    @JsonProperty("Nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonProperty("Quantidade")
    public Integer getQuantidade() {
        return quantidade;
    }

    @JsonProperty("Quantidade")
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    @JsonProperty("ValorProduto")
    public Double getValorProduto() {
        return valorProduto;
    }

    @JsonProperty("ValorProduto")
    public void setValorProduto(Double valorProduto) {
        this.valorProduto = valorProduto;
    }

    @JsonProperty("ValorReal")
    public Double getValorReal() {
        return valorReal;
    }

    @JsonProperty("ValorReal")
    public void setValorReal(Double valorReal) {
        this.valorReal = valorReal;
    }

    @JsonProperty("ValorEmbalagem")
    public Double getValorEmbalagem() {
        return valorEmbalagem;
    }

    @JsonProperty("ValorEmbalagem")
    public void setValorEmbalagem(Double valorEmbalagem) {
        this.valorEmbalagem = valorEmbalagem;
    }

}

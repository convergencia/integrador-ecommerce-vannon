package io.convergencia.integra.vannon.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Ativo",
    "TipoPessoa",
    "IeIsento",
    "Sexo",
    "AceitaMailing",
    "AceitaSms",
    "CodigoExterno",
    "DataNascimento",
    "Nome",
    "Apelido",
    "CpfCnpj",
    "RG",
    "RazaoSocial",
    "InscricaoEstadual",
    "Sobrenome",
    "Responsavel",
    "Email",
    "Senha",
    "Telefone",
    "Celular"
})
public class Cliente {

    @JsonProperty("Ativo")
    private Boolean ativo;
    @JsonProperty("TipoPessoa")
    private String tipoPessoa;
    @JsonProperty("IeIsento")
    private Boolean ieIsento;
    @JsonProperty("Sexo")
    private String sexo;
    @JsonProperty("AceitaMailing")
    private Boolean aceitaMailing;
    @JsonProperty("AceitaSms")
    private Boolean aceitaSms;
    @JsonProperty("CodigoExterno")
    private String codigoExterno;
    @JsonProperty("DataNascimento")
    private String dataNascimento;
    @JsonProperty("Nome")
    private String nome;
    @JsonProperty("Apelido")
    private String apelido;
    @JsonProperty("CpfCnpj")
    private String cpfCnpj;
    @JsonProperty("RG")
    private String rG;
    @JsonProperty("RazaoSocial")
    private String razaoSocial;
    @JsonProperty("InscricaoEstadual")
    private String inscricaoEstadual;
    @JsonProperty("Sobrenome")
    private String sobrenome;
    @JsonProperty("Responsavel")
    private String responsavel;
    @JsonProperty("Email")
    private String email;
    @JsonProperty("Senha")
    private String senha;
    @JsonProperty("Telefone")
    private String telefone;
    @JsonProperty("Celular")
    private String celular;

    @JsonProperty("Ativo")
    public Boolean getAtivo() {
        return ativo;
    }

    @JsonProperty("Ativo")
    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @JsonProperty("TipoPessoa")
    public String getTipoPessoa() {
        return tipoPessoa;
    }

    @JsonProperty("TipoPessoa")
    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    @JsonProperty("IeIsento")
    public Boolean getIeIsento() {
        return ieIsento;
    }

    @JsonProperty("IeIsento")
    public void setIeIsento(Boolean ieIsento) {
        this.ieIsento = ieIsento;
    }

    @JsonProperty("Sexo")
    public String getSexo() {
        return sexo;
    }

    @JsonProperty("Sexo")
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @JsonProperty("AceitaMailing")
    public Boolean getAceitaMailing() {
        return aceitaMailing;
    }

    @JsonProperty("AceitaMailing")
    public void setAceitaMailing(Boolean aceitaMailing) {
        this.aceitaMailing = aceitaMailing;
    }

    @JsonProperty("AceitaSms")
    public Boolean getAceitaSms() {
        return aceitaSms;
    }

    @JsonProperty("AceitaSms")
    public void setAceitaSms(Boolean aceitaSms) {
        this.aceitaSms = aceitaSms;
    }

    @JsonProperty("CodigoExterno")
    public String getCodigoExterno() {
        return codigoExterno;
    }

    @JsonProperty("CodigoExterno")
    public void setCodigoExterno(String codigoExterno) {
        this.codigoExterno = codigoExterno;
    }

    @JsonProperty("DataNascimento")
    public String getDataNascimento() {
        return dataNascimento;
    }

    @JsonProperty("DataNascimento")
    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @JsonProperty("Nome")
    public String getNome() {
        return nome;
    }

    @JsonProperty("Nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonProperty("Apelido")
    public String getApelido() {
        return apelido;
    }

    @JsonProperty("Apelido")
    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    @JsonProperty("CpfCnpj")
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    @JsonProperty("CpfCnpj")
    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    @JsonProperty("RG")
    public String getRG() {
        return rG;
    }

    @JsonProperty("RG")
    public void setRG(String rG) {
        this.rG = rG;
    }

    @JsonProperty("RazaoSocial")
    public String getRazaoSocial() {
        return razaoSocial;
    }

    @JsonProperty("RazaoSocial")
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    @JsonProperty("InscricaoEstadual")
    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    @JsonProperty("InscricaoEstadual")
    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    @JsonProperty("Sobrenome")
    public String getSobrenome() {
        return sobrenome;
    }

    @JsonProperty("Sobrenome")
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    @JsonProperty("Responsavel")
    public String getResponsavel() {
        return responsavel;
    }

    @JsonProperty("Responsavel")
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    @JsonProperty("Email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("Email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("Senha")
    public String getSenha() {
        return senha;
    }

    @JsonProperty("Senha")
    public void setSenha(String senha) {
        this.senha = senha;
    }

    @JsonProperty("Telefone")
    public String getTelefone() {
        return telefone;
    }

    @JsonProperty("Telefone")
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @JsonProperty("Celular")
    public String getCelular() {
        return celular;
    }

    @JsonProperty("Celular")
    public void setCelular(String celular) {
        this.celular = celular;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author wanderson
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NumeroPedido",
    "Rastreamento"
})
public class PedidoRastreamento {

    @JsonProperty("NumeroPedido")
    private String numeroPedido;
    @JsonProperty("Rastreamento")
    private String rastreamento;

    @JsonProperty("NumeroPedido")
    public String getNumeroPedido() {
        return numeroPedido;
    }

    @JsonProperty("NumeroPedido")
    public void setNumeroPedido(String numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    @JsonProperty("Rastreamento")
    public String getRastreamento() {
        return rastreamento;
    }

    @JsonProperty("Rastreamento")
    public void setRastreamento(String rastreamento) {
        this.rastreamento = rastreamento;
    }
}

package io.convergencia.integra.vannon.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CodigoExterno",
    "Nome"
})
public class PedidoStatus {

    @JsonProperty("CodigoExterno")
    private String codigoExterno;
    @JsonProperty("Nome")
    private String nome;

    @JsonProperty("CodigoExterno")
    public String getCodigoExterno() {
        return codigoExterno;
    }

    @JsonProperty("CodigoExterno")
    public void setCodigoExterno(String codigoExterno) {
        this.codigoExterno = codigoExterno;
    }

    @JsonProperty("Nome")
    public String getNome() {
        return nome;
    }

    @JsonProperty("Nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.jobs;

import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.enuns.EStatusIntegracao;
import io.convergencia.integra.ecommerce.enuns.EStatusJob;
import io.convergencia.integra.ecommerce.intefaces.IEcommerceEnviaStatusPedido;
import io.convergencia.integra.ecommerce.model.InterfaceJob;
import io.convergencia.integra.ecommerce.model.JobLog;
import io.convergencia.integra.ecommerce.model.PedidoRastreamento;
import io.convergencia.integra.ecommerce.repository.InterfaceJobDao;
import io.convergencia.integra.ecommerce.repository.JobLogDao;
import io.convergencia.integra.ecommerce.repository.PedidoRastreamentoDao;

import java.util.Date;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
//import io.convergencia.integra.vannon.ws.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * @author wanderson
 */
@org.springframework.stereotype.Service
public class JobEnviaPedidoStautsEcommerce implements IEcommerceEnviaStatusPedido {

    @Autowired
    private Environment env;

    @Autowired
    private PedidoRastreamentoDao pedidoRastreamentoDao;

    @Autowired
    private InterfaceJobDao interfaceJobDao;

    //@Autowired
    //private Service service;

    @Autowired
    private JobLogDao jobLogDao;

    @Override
    @Scheduled(fixedDelay = 300_000L)
    public void doProcessar() {
        InterfaceJob interfaceJob = interfaceJobDao.findByNome(EInterfaces.ECOMMERCE_ENVIA_STATUS_PEDIDO.name());
        if (interfaceJob.isAtiva()) {
            if (!isProcessando()) {
                Iterator<PedidoRastreamento> stream = pedidoRastreamentoDao.findTop50ByStatusIntegracao(EStatusIntegracao.NAO_INTEGRADO).iterator();

                if (stream.hasNext()) {

                    JobLog jobLog = new JobLog();

                    jobLog.setJobName(EInterfaces.ECOMMERCE_ENVIA_STATUS_PEDIDO.name());
                    jobLog.setStatusJob(EStatusJob.PROCESSANDO);
                    jobLogDao.save(jobLog);

                    while (stream.hasNext()) {
                        do {
                            doSendStatusPedido(stream.next());
                        } while (stream.hasNext());
                        stream = pedidoRastreamentoDao.findTop50ByStatusIntegracao(EStatusIntegracao.NAO_INTEGRADO).iterator();
                    }

                    jobLog.setStatusJob(EStatusJob.PROCESSADO);
                    jobLogDao.save(jobLog);

                }
            }
            interfaceJob.setDataUltimaExecucao(new Date());
            interfaceJobDao.save(interfaceJob);

        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void doSendStatusPedido(PedidoRastreamento next) {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("IDSite", env.getProperty("webvenda.idsite", Integer.class, 0).toString());
        map.add("Senha", env.getProperty("webvenda.senha", ""));
        map.add("lngIDCompra", next.getPedido().getCodigoPedido().toString());
        map.add("Cod_Rastreamento_ECT", next.getObservacao());
        map.add("Status_Pedido", next.getCodigo().getChave());

        //service.notificaStatusPedido(map);

        next.setStatusIntegracao(EStatusIntegracao.INTEGRADO);

        pedidoRastreamentoDao.save(next);
    }

    @Override
    public boolean isProcessando() {
        JobLog ultimoJobExecutado = getUltimoJob();
        if (null != ultimoJobExecutado) {
            return ultimoJobExecutado.getStatusJob().equals(EStatusJob.PROCESSANDO);
        }
        return false;
    }

    @Override
    public JobLog getUltimoJob() {
        return jobLogDao.ultimoEnvio(EInterfaces.ECOMMERCE_ENVIA_STATUS_PEDIDO.name());
    }

    @Override
    public void initValoresParaProcesso() {
        //Verificar como fazer
    }

}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.jobs;

import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.enuns.EStatusIntegracao;
import io.convergencia.integra.ecommerce.enuns.EStatusJob;
import io.convergencia.integra.ecommerce.intefaces.IEcommerceEnviaProduto;
import io.convergencia.integra.ecommerce.model.InterfaceJob;
import io.convergencia.integra.ecommerce.model.JobLog;
import io.convergencia.integra.ecommerce.model.Produto;
import io.convergencia.integra.ecommerce.repository.EstoqueDao;
import io.convergencia.integra.ecommerce.repository.InterfaceJobDao;
import io.convergencia.integra.ecommerce.repository.JobLogDao;
import io.convergencia.integra.ecommerce.repository.ProdutoDao;
import io.convergencia.integra.vannon.enuns.EStatusEcommerce;
import io.convergencia.integra.vannon.repository.ErroWebVendasDao;
import io.convergencia.integra.vannon.ws.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author wanderson
 */
@org.springframework.stereotype.Service
public class JobEnviaProdutoEcommerce implements IEcommerceEnviaProduto {

    @Autowired
    private Environment env;

    @Autowired
    private ProdutoDao produtoDao;

    @Autowired
    private EstoqueDao estoqueDao;

    @Autowired
    private InterfaceJobDao interfaceJobDao;

    @Autowired
    private Service service;

    @Autowired
    private JobLogDao jobLogDao;

    @Autowired
    private ErroWebVendasDao erroWebVendasDao;

    @Override
    @Scheduled(fixedDelay = 300_000L)
    public void doProcessar() {
        InterfaceJob interfaceJob = interfaceJobDao.findByNome(EInterfaces.ECOMMERCE_ENVIA_PRODUTO.name());
        if (interfaceJob.isAtiva()) {
            if (!isProcessando()) {
                Iterator<Produto> it = produtoDao.findByStatusIntegracao(EStatusIntegracao.NAO_INTEGRADO).iterator(); // pendente de integracao nao integrado

                if (it.hasNext()) {

                    JobLog jobLog = new JobLog();

                    jobLog.setJobName(EInterfaces.ECOMMERCE_ENVIA_PRODUTO.name());
                    jobLog.setStatusJob(EStatusJob.PROCESSANDO);
                    jobLogDao.save(jobLog);

                    while (it.hasNext()) {

                        Collection<io.convergencia.integra.vannon.model.Produto> produtos = new ArrayList<>();

                        do {
                            Produto prodErp = it.next();

                            doGetProduto(prodErp, produtos);

                        } while (produtos.size() <= 500 && it.hasNext());

                        RetornoProduto rp = null;//service.enviarProduto(ep);

                        if (!rp.isErro()) {
                            produtos.forEach((prod) -> {
                                prod.setStatusIntegracao(EStatusIntegracao.INTEGRADO);
                            });

                        } else {
                            produtos.forEach((prod) -> {
                                prod.setStatusIntegracao(EStatusIntegracao.ERRO);
                            });

                            erroWebVendasDao.save(rp.getErroWebVenda());
                        }

                        produtoDao.save(produtos);

                    }

                    jobLog.setStatusJob(EStatusJob.PROCESSADO);
                    jobLog.setDataTermino(new Date());
                    jobLogDao.save(jobLog);
                }
            }
            interfaceJob.setDataUltimaExecucao(new Date());
            interfaceJobDao.save(interfaceJob);

        }

    }

    private void doGetProduto(Produto itm, Collection<io.convergencia.integra.vannon.model.Produto> produtos) {
        io.convergencia.integra.vannon.model.Produto p = new io.convergencia.integra.vannon.model.Produto();

        p.setNomeProduto(itm.getDescricao());
        p.setCodigoExterno(itm.getCodigo().toString());
        
        
        produtos.add(p);
    }

    @Override
    public boolean isProcessando() {
        JobLog ultimoJobExecutado = getUltimoJob();
        if (null != ultimoJobExecutado) {
            return ultimoJobExecutado.getStatusJob().equals(EStatusJob.PROCESSANDO);
        }
        return false;
    }

    @Override
    public JobLog getUltimoJob() {
        return jobLogDao.ultimoEnvio(EInterfaces.ECOMMERCE_ENVIA_PRODUTO.name());
    }

    @Override
    public void initValoresParaProcesso() {
        produtoDao.resetFullIntegracao();
    }

}

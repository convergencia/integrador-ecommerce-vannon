/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.jobs;

import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.enuns.EStatusIntegracao;
import io.convergencia.integra.ecommerce.enuns.EStatusJob;
import io.convergencia.integra.ecommerce.intefaces.IEcommerceEnviaPreco;
import io.convergencia.integra.ecommerce.model.Estoque;
import io.convergencia.integra.ecommerce.model.InterfaceJob;
import io.convergencia.integra.ecommerce.model.JobLog;
import io.convergencia.integra.ecommerce.repository.EstoqueDao;
import io.convergencia.integra.ecommerce.repository.InterfaceJobDao;
import io.convergencia.integra.ecommerce.repository.JobLogDao;
import io.convergencia.integra.vannon.repository.ErroWebVendasDao;
import io.convergencia.integra.vannon.traffic.entidades.Authentic;
import io.convergencia.integra.vannon.traffic.entidades.EnvioProduto;
import io.convergencia.integra.vannon.traffic.entidades.RetornoProduto;
//import io.convergencia.integra.vannon.ws.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author wanderson
 */
@org.springframework.stereotype.Service
public class JobEnviaPrecoEcommerce implements IEcommerceEnviaPreco {

    @Autowired
    private Environment env;

    @Autowired
    private EstoqueDao estoqueDao;

    @Autowired
    private InterfaceJobDao interfaceJobDao;

    //@Autowired
    //private Service service;

    @Autowired
    private JobLogDao jobLogDao;

    @Autowired
    private ErroWebVendasDao erroWebVendasDao;

    @Override
    @Scheduled(fixedDelay = 60_000L)
    public void doProcessar() {
        InterfaceJob interfaceJob = interfaceJobDao.findByNome(EInterfaces.ECOMMERCE_ENVIA_PRECO.name());
        if (interfaceJob.isAtiva()) {
            if (!isProcessando()) {
                List<Estoque> stream = estoqueDao.findTop500ByStatusIntegracaoPrecoOrderByDataUltimaIntegracaoPrecoAsc(EStatusIntegracao.NAO_INTEGRADO);

                if (!stream.isEmpty()) {

                    JobLog jobLog = new JobLog();

                    jobLog.setJobName(EInterfaces.ECOMMERCE_ENVIA_PRECO.name());
                    jobLog.setStatusJob(EStatusJob.PROCESSANDO);
                    jobLogDao.save(jobLog);

                    try{
                    doSendPreco(stream);
                    } catch(Exception e){
                        e.printStackTrace(System.out);
                    }
                    jobLog.setStatusJob(EStatusJob.PROCESSADO);
                    jobLog.setDataTermino(new Date());
                    jobLogDao.save(jobLog);

                }
            }
            interfaceJob.setDataUltimaExecucao(new Date());
            interfaceJobDao.save(interfaceJob);

        }

    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void doSendPreco(List<Estoque> stream) {

        EnvioProduto ep = new EnvioProduto();
        ep.setAuthentic(new Authentic());
        ep.getAuthentic().setFonte(env.getProperty("webvenda.fonte", ""));
        ep.getAuthentic().setIdSite(env.getProperty("webvenda.idsite", Integer.class, 0));
        ep.getAuthentic().setSenha(env.getProperty("webvenda.senha", ""));

        stream.forEach((est) -> {
            io.convergencia.integra.vannon.entidades.Produto p = new io.convergencia.integra.vannon.entidades.Produto();

            p.setId(est.getProduto().getCodigo());
            p.setPreco(est.getPrecoVenda());
            p.setPrecoPromocional(est.getPrecoOferta());
            p.setCodigoEan(est.getProduto().getEan());

            ep.add(p);

            est.setPrecoVendaIntegrado(est.getPrecoVenda());
            est.setPrecoOfertaIntegrado(est.getPrecoOferta());
            est.setDataUltimaIntegracaoPreco(new Date());

            est.setStatusIntegracaoPreco(EStatusIntegracao.INTEGRADO);
        });

        RetornoProduto rp = null;//service.enviarPreco(ep);

        if (rp.isErro()) {
            rp.getProdutosErro().forEach((prodErro) -> {

                stream.forEach((est) -> {
                    if (prodErro.getCodProduto().equals(est.getProduto().getCodigo())) {
                        est.setStatusIntegracaoPreco(EStatusIntegracao.ERRO);
                    }
                });

            });

            erroWebVendasDao.save(rp.getErroWebVenda());
        }

        for (Estoque estoque : stream) {
            doPersistAlter(estoque);
        }
    }

    public void doPersistAlter(Estoque estoque) {
        estoqueDao.updatePreco(estoque.getStatusIntegracaoEstoque(), estoque.getPrecoVendaIntegrado(), estoque.getPrecoOfertaIntegrado(), estoque.getDataUltimaIntegracaoPreco(), estoque.getId());
    }

    @Override
    public boolean isProcessando() {
        JobLog ultimoJobExecutado = getUltimoJob();
        if (null != ultimoJobExecutado) {
            return ultimoJobExecutado.getStatusJob().equals(EStatusJob.PROCESSANDO);
        }
        return false;
    }

    @Override
    public JobLog getUltimoJob() {
        return jobLogDao.ultimoEnvio(EInterfaces.ECOMMERCE_ENVIA_PRECO.name());
    }

    @Override
    public void initValoresParaProcesso() {
        estoqueDao.resetFullIntegracaoPreco();
    }

}

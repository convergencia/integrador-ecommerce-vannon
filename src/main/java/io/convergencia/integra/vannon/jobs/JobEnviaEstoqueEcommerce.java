/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.jobs;

import io.convergencia.integra.ecommerce.enuns.EStatusIntegracao;
import io.convergencia.integra.ecommerce.enuns.EStatusJob;
import io.convergencia.integra.ecommerce.model.Estoque;
import io.convergencia.integra.ecommerce.model.InterfaceJob;
import io.convergencia.integra.ecommerce.model.JobLog;
import io.convergencia.integra.ecommerce.repository.EstoqueDao;
import io.convergencia.integra.ecommerce.repository.InterfaceJobDao;
import io.convergencia.integra.ecommerce.repository.JobLogDao;
import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.intefaces.IEcommerceEnviaEstoque;
import io.convergencia.integra.vannon.model.Produto;
import io.convergencia.integra.vannon.model.Sku;
import io.convergencia.integra.vannon.repository.ErroWebVendasDao;
import io.convergencia.integra.vannon.ws.Service;
import java.util.ArrayList;
import java.util.Collection;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author wanderson
 */
@org.springframework.stereotype.Service
public class JobEnviaEstoqueEcommerce implements IEcommerceEnviaEstoque {

    @Autowired
    private Environment env;

    @Autowired
    private EstoqueDao estoqueDao;

    @Autowired
    private InterfaceJobDao interfaceJobDao;

    @Autowired
    private Service service;
    
    @Autowired
    private JobLogDao jobLogDao;

    @Autowired
    private ErroWebVendasDao erroWebVendasDao;

    @Override
    @Scheduled(fixedDelay = 60_000L)
    public void doProcessar() {
        InterfaceJob interfaceJob = interfaceJobDao.findByNome(EInterfaces.ECOMMERCE_ENVIA_ESTOQUE.name());

        if (interfaceJob.isAtiva()) {
            if (!isProcessando()) {
                List<Estoque> stream = estoqueDao.findTop500ByStatusIntegracaoEstoqueOrderByDataUltimaIntegracaoEstoqueAsc(EStatusIntegracao.NAO_INTEGRADO);

                if (!stream.isEmpty()) {

                    JobLog jobLog = new JobLog();

                    jobLog.setJobName(EInterfaces.ECOMMERCE_ENVIA_ESTOQUE.name());
                    jobLog.setStatusJob(EStatusJob.PROCESSANDO);
                    jobLogDao.save(jobLog);

                    try {
                        doSendEstoque(stream);
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                    }

                    jobLog.setStatusJob(EStatusJob.PROCESSADO);
                    jobLog.setDataTermino(new Date());
                    jobLogDao.save(jobLog);

                }
            }
            interfaceJob.setDataUltimaExecucao(new Date());
            interfaceJobDao.save(interfaceJob);

        }

    }

    public void doSendEstoque(List<Estoque> stream) {

        Collection<Sku> produtosEStoque = new ArrayList<>();

        stream.forEach((est) -> {
            
            Sku p = new Sku();
            

            p.setCodigoExterno(est.getProduto().getCodigo().toString());
            p.set(est.getQuantidade().intValue());

            ep.add(p);

            est.setQuantidadeIntegrado(est.getQuantidade());
            est.setDataUltimaIntegracaoEstoque(new Date());
            est.setStatusIntegracaoEstoque(EStatusIntegracao.INTEGRADO);
        });

        RetornoProduto rp = null;//service.enviarEstoque(ep);

        if (rp.isErro()) {

            rp.getProdutosErro().forEach((prodErro) -> {

                stream.forEach((est) -> {
                    if (prodErro.getCodProduto().equals(est.getProduto().getCodigo())) {
                        est.setStatusIntegracaoEstoque(EStatusIntegracao.ERRO);
                    }
                });

            });

            erroWebVendasDao.save(rp.getErroWebVenda());
        }

        stream.forEach((estoque) -> {
            doPersistAlter(estoque);
        });
    }

    public void doPersistAlter(Estoque estoque) {
        estoqueDao.updateEstoque(estoque.getStatusIntegracaoEstoque(), estoque.getQuantidadeIntegrado(), estoque.getDataUltimaIntegracaoEstoque(), estoque.getId());
    }

    @Override
    public boolean isProcessando() {
        JobLog ultimoJobExecutado = getUltimoJob();
        if (null != ultimoJobExecutado) {
            return ultimoJobExecutado.getStatusJob().equals(EStatusJob.PROCESSANDO);
        }
        return false;
    }

    @Override
    public JobLog getUltimoJob() {
        return jobLogDao.ultimoEnvio(EInterfaces.ECOMMERCE_ENVIA_ESTOQUE.name());
    }

    @Override
    public void initValoresParaProcesso() {
        estoqueDao.resetFullIntegracaoEstoque();
    }

}

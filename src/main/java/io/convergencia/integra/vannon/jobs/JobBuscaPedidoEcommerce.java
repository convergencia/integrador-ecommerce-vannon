/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.jobs;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.enuns.EStatusJob;
import io.convergencia.integra.ecommerce.enuns.ETipoMessage;
import io.convergencia.integra.ecommerce.intefaces.IEcommerceBuscaPedido;
import io.convergencia.integra.ecommerce.model.Cliente;
import io.convergencia.integra.ecommerce.model.InterfaceJob;
import io.convergencia.integra.ecommerce.model.JobLog;
import io.convergencia.integra.ecommerce.model.Pedido;
import io.convergencia.integra.ecommerce.model.PedidoNotificacao;
import io.convergencia.integra.ecommerce.model.Produto;
import io.convergencia.integra.ecommerce.repository.ClienteDao;
import io.convergencia.integra.ecommerce.repository.InterfaceJobDao;
import io.convergencia.integra.ecommerce.repository.JobLogDao;
import io.convergencia.integra.ecommerce.repository.PedidoDao;
import io.convergencia.integra.ecommerce.repository.ProdutoDao;
import io.convergencia.integra.vannon.ws.Service;
import org.hibernate.boot.jaxb.internal.stax.LocalXmlResourceResolver;

/**
 * @author wanderson
 */
@org.springframework.stereotype.Service
public class JobBuscaPedidoEcommerce implements IEcommerceBuscaPedido {

    @Autowired
    private Environment env;

    @Autowired
    private PedidoDao pedidoDao;

    @Autowired
    private ProdutoDao produtoDao;

    @Autowired
    private ClienteDao clienteDao;

    @Autowired
    private InterfaceJobDao interfaceJobDao;

    @Autowired
    private Service service;

    @Autowired
    private JobLogDao jobLogDao;

    @Override
    @Scheduled(fixedDelay = 300_000L)
    public void doProcessar() {

        InterfaceJob interfaceJob = interfaceJobDao.findByNome(EInterfaces.ECOMMERCE_BUSCA_PEDIDO.name());

        if (interfaceJob.isAtiva()) {

            Collection<io.convergencia.integra.vannon.model.Pedido> buscarProduto = service.getPedidos();

            buscarProduto.forEach((pedido) -> {
                doProcessaPedido(pedido);
            });
            interfaceJob.setDataUltimaExecucao(new Date());
            interfaceJobDao.save(interfaceJob);

        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void doProcessaPedido(io.convergencia.integra.vannon.model.Pedido pedido) {

        Pedido pExistente = pedidoDao.findOneByCodigoPedido(Long.parseLong(pedido.getNumeroPedido()));

        if (pExistente == null) {

            parsePedido(pedido);
            
            
            JobLog jobLog = new JobLog();

            jobLog.setJobName(EInterfaces.ECOMMERCE_BUSCA_PEDIDO.name());
            jobLog.setStatusJob(EStatusJob.PROCESSANDO);
            jobLogDao.save(jobLog);

            Cliente cli = clienteDao.findOneByCnpjCpf(pedido.getCliente().getCpfCnpj());
            if (cli == null) {
                clienteDao.save(pedido.getCliente());
            } else {
                pedido.setCliente(cli);
            }

            pedido.getPedidoProdutos().forEach((p) -> {
                Produto pr = p.getProduto();
                Produto produto = produtoDao.findOneByCodigo(pr.getCodigo());

                if (produto == null) {
                    pedido.getNotificacoes().add(new PedidoNotificacao(pedido, String.format("Produto %s-%s não exitente no integrador", pr.getCodigo(), pr.getDescricao()), ETipoMessage.ERRO_ECOMERCE));

                }
                p.setProduto(produto);

            });

            pedidoDao.save(pedido);

            jobLog.setMessagem(String.format("Pedido %s integrado sobre o codigo %s", pedido.getCodigoPedido(), pedido.getId()));
            jobLog.setStatusJob(EStatusJob.PROCESSADO);
            jobLog.setDataTermino(new Date());
            jobLogDao.save(jobLog);

        }

        
    }

    @Override
    public boolean isProcessando() {
        JobLog ultimoJobExecutado = getUltimoJob();
        if (null != ultimoJobExecutado) {
            return ultimoJobExecutado.getStatusJob().equals(EStatusJob.PROCESSANDO);
        }
        return false;
    }

    @Override
    public JobLog getUltimoJob() {
        return jobLogDao.ultimoEnvio(EInterfaces.ECOMMERCE_BUSCA_PEDIDO.name());
    }

    @Override
    public void initValoresParaProcesso() {
        //Nao existe esta operacao
    }

    private Pedido parsePedido(io.convergencia.integra.vannon.model.Pedido pedido) {
        Pedido p = new Pedido();
        
        p.setCodigoPedido(Long.parseLong(pedido.getNumeroPedido()));
        p.setDataCompra(LocalXmlResourceResolver.DtdMapping.);
        
        
        
        return p;    
    }

}

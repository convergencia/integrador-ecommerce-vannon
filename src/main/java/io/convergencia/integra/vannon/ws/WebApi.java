/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.ws;

import feign.Feign;
import feign.Headers;
import feign.RequestInterceptor;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import io.convergencia.integra.vannon.model.Pedido;
import io.convergencia.integra.vannon.model.PedidoRastreamento;
import io.convergencia.integra.vannon.model.PedidoStatus;
import io.convergencia.integra.vannon.model.Produto;
import io.convergencia.integra.vannon.model.Sku;
import java.util.Collection;
import org.springframework.stereotype.Component;

/**
 *
 * @author wanderson
 */
@Component
@Headers("Content-Type: application/x-www-form-urlencoded")
public interface WebApi {

    @RequestLine("POST /produto")
    String enviarProduto(Collection<Produto> produtos);

    @RequestLine("PUT /produto")
    String alterarProduto(Collection<Produto> produtos);

    @RequestLine("PATCH /produto")
    String alterarProdutoParcial(Collection<Produto> produtos);

    @RequestLine("POST /sku")
    String enviarProdutoSku(Collection<Sku> sku);

    @RequestLine("PUT /produto")
    String alterarProdutoSku(Collection<Sku> sku);

    @RequestLine("PATCH /produto")
    String alterarProdutoParcialSlu(Collection<Sku> sku);

    @RequestLine("GET /pedido?$filter=Integracao ne 'I'")
    Collection<Pedido> getPedidos();

    @RequestLine("POST /pedidostatus")
    String alterarStatusPedido(Collection<PedidoStatus> pedidosStatus);

    @RequestLine("POST /pedidorastreamento")
    String alterarRastreamentoPedido(Collection<PedidoRastreamento> pedidosRastreamentos);

    static WebApi connect(RequestInterceptor interceptor) {
        return Feign.builder()
                .requestInterceptor(interceptor)
                .decoder(new JacksonDecoder())
                .encoder(new JacksonEncoder())
                .target(WebApi.class, "http://integracao.itec.vfarma.com.br");
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.ws;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 *
 * @author wanderson
 */
public class ApiKeyRequestInterceptor implements RequestInterceptor {

    private final String location;
    private final String name;
    private final String value;

    public ApiKeyRequestInterceptor(String location, String name, String value) {

        this.location = location;
        this.name = name;
        this.value = value;
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        if (location.equals("header")) {
            requestTemplate.header(name, value);
        } else if (location.equals("query")) {
            requestTemplate.query(name, value);
        }
    }

}

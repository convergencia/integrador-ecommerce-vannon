/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.ws;

import io.convergencia.integra.vannon.model.Pedido;
import io.convergencia.integra.vannon.model.Produto;
import java.util.Arrays;
import java.util.Collection;

/**
 *
 * @author wanderson
 */
@org.springframework.stereotype.Service
public class Service {

    private final ApiKeyRequestInterceptor interceptor;
    private final WebApi webApi;

    public Service() {
        interceptor = new ApiKeyRequestInterceptor("header", "Authorization", "piKey dan: {apiKey}");
        webApi = WebApi.connect(interceptor);
    }

    public void enviarProduto(Produto... produto) {
        webApi.enviarProduto(Arrays.asList(produto));
    }

    public Collection<Pedido> getPedidos() {
        return webApi.getPedidos();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.web.rest;

import io.convergencia.integra.vannon.model.ErroWebVenda;
import io.convergencia.integra.vannon.repository.ErroWebVendasDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wanderson
 */
@RestController
@RequestMapping(value = "/api/webvenda/erro")
public class ErroWebVendaController {
    @Autowired
    private ErroWebVendasDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<ErroWebVenda> person() {
        List<ErroWebVenda> target = new ArrayList<>();
        dao.findTop150ByOrderByIdDesc().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/clean-all", method = RequestMethod.DELETE)
    public ResponseEntity<Void> cleanAll() {
        dao.deleteAll();
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ErroWebVenda person(@PathVariable Long id) {
        return dao.findOne(id);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.web.rest;

import io.convergencia.integra.ecommerce.model.Pedido;
import io.convergencia.integra.ecommerce.repository.PedidoDao;
//import io.convergencia.integra.vannon.ws.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wanderson
 */
@RestController
@RequestMapping(value = "/api/pedido")
public class NotificationController {
    
    @Autowired
    private Environment env;

    @Autowired
    private PedidoDao dao;
    
    //@Autowired
    //private Service service;
    
    @RequestMapping(value = "/re-integrar/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> reIntegrarPedidoEcommerce(@PathVariable Long id) {

        Pedido p = dao.findOne(id);

 
        //service.notificaIntegrado(map);

        dao.delete(id);

        return ResponseEntity.ok(null);
    }
}

package io.convergencia.integra.vannon.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.convergencia.integra.ecommerce.enuns.EInterfaces;
import io.convergencia.integra.ecommerce.intefaces.IJobProcesso;
import io.convergencia.integra.vannon.jobs.JobBuscaPedidoEcommerce;
import io.convergencia.integra.vannon.jobs.JobEnviaEstoqueEcommerce;
import io.convergencia.integra.vannon.jobs.JobEnviaPedidoStautsEcommerce;
import io.convergencia.integra.vannon.jobs.JobEnviaPrecoEcommerce;
import io.convergencia.integra.vannon.jobs.JobEnviaProdutoEcommerce;

/**
 * Created by wanderson on 07/03/17.
 */


@Component
public class JobFactoryWebVendas {

    @Autowired
    private JobBuscaPedidoEcommerce jobBuscaPedidoEcommerce;

    @Autowired
    private JobEnviaEstoqueEcommerce jobEnviaEstoqueEcommerce;

    @Autowired
    private JobEnviaPedidoStautsEcommerce jobEnviaPedidoStautsEcommerce;

    @Autowired
    private JobEnviaPrecoEcommerce jobEnviaPrecoEcommerce;

    @Autowired
    private JobEnviaProdutoEcommerce jobEnviaProdutoEcommerce;

    public IJobProcesso getJob(EInterfaces e) {

        switch (e) {
            case ECOMMERCE_BUSCA_PEDIDO:
                return jobBuscaPedidoEcommerce;
            case ECOMMERCE_ENVIA_ESTOQUE:
                return jobEnviaEstoqueEcommerce;
            case ECOMMERCE_ENVIA_PRECO:
                return jobEnviaPrecoEcommerce;
            case ECOMMERCE_ENVIA_PRODUTO:
                return jobEnviaProdutoEcommerce;
            case ECOMMERCE_ENVIA_STATUS_PEDIDO:
                return jobEnviaPedidoStautsEcommerce;

            default:
                throw new RuntimeException("Interface não é de processamento WebVendas");
        }

    }

}

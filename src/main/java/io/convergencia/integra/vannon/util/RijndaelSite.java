/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author wanderson
 */
public class RijndaelSite {

    private static RijndaelSite INSTANCIA;

    private final byte[] iv = new byte[]{121, (byte) 241, 10, 1, (byte) 132, 74, 11, 39, (byte) 255, 91, 45, 78, 14, (byte) 211, 22, 62};

    private final Cipher cipherEnc;
    private final Cipher cipherDec;

    public static final RijndaelSite getInstance(String key) {
        if (INSTANCIA == null) {
            INSTANCIA = new RijndaelSite(key);
        }
        return INSTANCIA;
    }

    private RijndaelSite(String key) {
        try {
            this.cipherDec = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.cipherDec.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes("ASCII"), "AES"), new IvParameterSpec(this.iv));
            this.cipherEnc = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.cipherEnc.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getBytes("ASCII"), "AES"), new IvParameterSpec(this.iv));

        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public String crypt(String textoDescriptografado) {
        try {
            return Base64.getEncoder().encodeToString(this.cipherEnc.doFinal(textoDescriptografado.getBytes()));
        } catch (IllegalBlockSizeException | BadPaddingException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String decrypt(String textoCriptografado) {
        try {
            return new String(cipherDec.doFinal(Base64.getDecoder().decode(textoCriptografado)));
        } catch (IllegalBlockSizeException | BadPaddingException ex) {
            throw new RuntimeException(ex);
        }
    }

}

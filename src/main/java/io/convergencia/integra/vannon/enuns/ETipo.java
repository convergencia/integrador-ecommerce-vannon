/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.enuns;

/**
 *
 * @author wanderson
 */
public enum ETipo {
    MEDICAMENTNO(1),
    OUTRO(2),
    GENERICO(3),
    SIMILAR(4),
    MANIPULADO(5);

    private final int code;

    private ETipo(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static ETipo fromCode(int code) {
        for (ETipo b : ETipo.values()) {
            if (b.getCode() == code) {
                return b;
            }
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.enuns;

/**
 *
 * @author wanderson
 */
public enum EStatusEcommerce {

    EXIBIR_SEM_RESTRICAO(11),
    EXIBIR_COM_RESTRICAO(9),
    NAO_EXIBIR(0);

    private final int code;

    private EStatusEcommerce(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static EStatusEcommerce fromCode(int code) {
        for (EStatusEcommerce b : EStatusEcommerce.values()) {
            if (b.getCode() == code) {
                return b;
            }
        }
        return null;
    }

}

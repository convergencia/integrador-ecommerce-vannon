/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.enuns;

/**
 *
 * @author wanderson
 */
public enum EControle {

    CONTROLADO(1),
    CONTROLE_ESPECIAL(2),
    NAO_HA(3);

    private final int code;

    private EControle(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static EControle fromCode(int code) {
        for (EControle b : EControle.values()) {
            if (b.getCode() == code) {
                return b;
            }
        }
        return null;
    }
}

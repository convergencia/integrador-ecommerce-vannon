/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.enuns;

/**
 *
 * @author wanderson
 */
public enum EFamilia {
    TERMOSSENSIVEL(1),
    TARJADO(2),
    GENERICO_TARJADO(3),
    TARJA_PRETA(4),
    ANTIBIOTICO(5),
    PSICOTROPICO(6),
    NAO_HA(7),
    GENERICO_TARJA_PRETA(8),
    ANTIBIOTICO_GENERICO(9);

    private final int code;

    private EFamilia(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static EFamilia fromCode(int code) {
        for (EFamilia b : EFamilia.values()) {
            if (b.getCode() == code) {
                return b;
            }
        }
        return null;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.enuns;

/**
 *
 * @author wanderson
 */
public enum TFormaPagamento {

    BOLETO(2),
    VISA(3),
    MASTER_CARD(4),
    AMERICAN_EXPRESS(5),
    DINERS(6),
    ELO(7),
    DEPOSITO(8),
    DINHEIRO(9);

    private final int code;

    private TFormaPagamento(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static TFormaPagamento fromCode(int code) {
        for (TFormaPagamento b : TFormaPagamento.values()) {
            if (b.getCode() == code) {
                return b;
            }
        }
        return null;
    }
}

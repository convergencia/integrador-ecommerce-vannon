/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.vannon.enuns;

/**
 *
 * @author wanderson
 */
public enum EStatus {
    CARRINHO("CAR"),
    AGUARDANDO_CONFIRMACAO_DE_PAGAMENTO("ACP"),
    CANCELADO("CAN"),
    ENTREGUE("ENT"),
    POSTADO("POS"),
    SEPARADO_PARA_POSTAGEM("SPP"),
    PAGAMENTO_CONFIRMADO("PCO"),
    PAGAMENTO_NAO_AUTORIZADO("PNA"),
    NAO_AUTORIZADO_AF("NAA"),
    PAGAMENTO_CONFIRMADO_BOLETO("PCB"),
    PEDIDO_EM_ANALISE("PAN"),
    AGUARDANDO_APROVACAO("AGA"),
    DISPONIVEL_PARA_RETIRADA("DPR");

    private final String code;

    private EStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static EStatus fromCode(String code) {
        for (EStatus b : EStatus.values()) {
            if (b.getCode().equalsIgnoreCase(code)) {
                return b;
            }
        }
        return null;
    }
}
